import { shallowMount } from "@vue/test-utils";
import Add from "@/components/Add.vue";

test("Este el Button de anadir ", async () => {
  const Contact = {
    id: "11",
    name: "Haimar",
    lastname: "Arregi",
    email: "haimar@example.com",
    phone: "000000000"
  };
  const wrapper = shallowMount(Add, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.id = Contact.id;
  wrapper.vm.name = Contact.name;
  wrapper.vm.lastname = Contact.lastname;
  wrapper.vm.email = Contact.email;
  wrapper.vm.phone = Contact.phone;

  const button = wrapper.find(".btnadd");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "contactadd",
      args: ["11", "Haimar", "Arregi", "haimar@example.com", "000000000"]
    }
  ]);
});

test("Este el Button de Volver ", async () => {
  const wrapper = shallowMount(Add);
  expect(wrapper.emitted().btnreturn).toBe(undefined);

  const button = wrapper.find(".volver");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().btnreturn.length).toBe(1);
  expect(wrapper.emitted().btnreturn[0]).toEqual([]);
});
