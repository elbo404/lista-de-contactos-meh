import { shallowMount } from "@vue/test-utils";
import Contactlistmodification from "@/components/Contactlistmodification.vue";
import Modificationcontacts from "@/components/Modificationcontacts.vue";

test("pinta la tabla de contactos", () => {
  const wrapper = shallowMount(Contactlistmodification, {
    propsData: {
      Contact: {
        id: null,
        name: null,
        lastname: null,
        email: null,
        phone: null
      }
    }
  });
  expect(wrapper.text()).toContain("");
  expect(wrapper.text()).toContain("");
  expect(wrapper.text()).toContain("");
  expect(wrapper.text()).toContain("");
});

test("Este el Button de Eliminar ", async () => {
  const Contact = {
    id: "13",
    name: "Elbo",
    lastname: "mohammed",
    email: "Elbo@example.com",
    phone: "0000000000"
  };
  const wrapper = shallowMount(Contactlistmodification, {
    propsData: { Contact: Contact }
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(Modificationcontacts);
  button.vm.$emit("btndelet", "13");
  await wrapper.vm.$nextTick();

  expect(wrapper.emittedByOrder()).toEqual([
    { name: "buttondelete", args: ["13"] }
  ]);
});

test("Este el Button de Save ", async () => {
  const Contact = {
    id: "13",
    name: "Elbo",
    lastname: "mohammed",
    email: "Elbo@example.com",
    phone: "0000000000"
  };
  const wrapper = shallowMount(Contactlistmodification, {
    propsData: { Contact: Contact }
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(Modificationcontacts);
  button.vm.$emit(
    "btnedit",
    "13",
    "Elbo",
    "mohammed",
    "Elbo@example.com",
    "0000000000"
  );
  await wrapper.vm.$nextTick();

  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "buttonedit",
      args: ["13", "Elbo", "mohammed", "Elbo@example.com", "0000000000"]
    }
  ]);
});

test("Este el Button de Añadir ", async () => {
  const wrapper = shallowMount(Contactlistmodification);
  expect(wrapper.emitted().BtnAdd).toBe(undefined);

  const button = wrapper.find(".add");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().BtnAdd.length).toBe(1);
  expect(wrapper.emitted().BtnAdd[0]).toEqual([]);
});

test("Este el Button de desconexión ", async () => {
  const wrapper = shallowMount(Contactlistmodification);
  expect(wrapper.emitted().btndeconct).toBe(undefined);

  const button = wrapper.find(".deconcte");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().btndeconct.length).toBe(1);
  expect(wrapper.emitted().btndeconct[0]).toEqual([]);
});
