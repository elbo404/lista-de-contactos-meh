import { shallowMount } from "@vue/test-utils";
import Modificationcontacts from "@/components/Modificationcontacts.vue";
import Contactlistmodification from "@/components/Contactlistmodification.vue";

test("pinta todo list vacío", () => {
  const wrapper = shallowMount(Modificationcontacts, {
    propsData: {
      Contact: []
    }
  });
  const listacontact = wrapper.findAll(Contactlistmodification).wrappers;

  expect(listacontact.length).toBe(0);
});

test("Este el Button de Eliminar ", async () => {
  const Contact = {
    id: "11",
    name: "Haimar",
    lastname: "Arregi",
    email: "haimar@example.com",
    phone: "000000000"
  };
  const wrapper = shallowMount(Modificationcontacts, {
    propsData: { Contact: Contact }
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(".btndel");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    { name: "btndelet", args: [Contact.id] }
  ]);
});

test("Este el Button de save ", async () => {
  const Contact = {
    id: "12",
    name: "Mohammed",
    lastname: "Sehili",
    email: "Mohammed@example.com",
    phone: "0000000000"
  };
  const wrapper = shallowMount(Modificationcontacts, {
    propsData: { Contact: Contact }
  });
  expect(wrapper.emittedByOrder()).toEqual([]);

  const button = wrapper.find(
    ".btnSave",
    "12",
    "Mohammed",
    "Sehili",
    "Mohammed@example.com",
    "0000000000"
  );
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "btnedit",
      args: ["12", "Mohammed", "Sehili", "Mohammed@example.com", "0000000000"]
    }
  ]);
});
