import { shallowMount } from "@vue/test-utils";
import Login from "@/components/Login.vue";

test("Este el Button de anadir ", async () => {
  const Contact = {
    user: "Admin",
    password: "Admin"
  };
  const wrapper = shallowMount(Login, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.user = Contact.user;
  wrapper.vm.password = Contact.user;

  const button = wrapper.find(".btnlogin");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "buttonlogin",
      args: ["Admin", "Admin"]
    }
  ]);
});
