import { shallowMount } from "@vue/test-utils";
import FirstScreen from "@/components/FirstScreen.vue";

test("Este el Button de administrator ", async () => {
  const wrapper = shallowMount(FirstScreen);
  expect(wrapper.emitted().administrator).toBe(undefined);

  const button = wrapper.find(".button");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().administrator.length).toBe(1);
  expect(wrapper.emitted().administrator[0]).toEqual([]);
});

test("Este el Button de Ver Contactos ", async () => {
  const wrapper = shallowMount(FirstScreen);

  expect(wrapper.emitted().vercontactos).toBe(undefined);

  const buttonver = wrapper.find(".btnver");
  buttonver.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted().vercontactos.length).toBe(1);
  expect(wrapper.emitted().vercontactos[0]).toEqual([]);
});
