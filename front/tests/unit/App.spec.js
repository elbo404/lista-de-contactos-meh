import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";
//import Listacontactosmodification from "@/components/Listacontactosmodification.vue";

test("El button de Administrator", async () => {
  const wrapper = shallowMount(App);

  expect(wrapper.emitted().administrator).toBe(undefined);

  const button = wrapper.find(".firstScreen");
  button.vm.$emit("administrator");
  await wrapper.vm.$nextTick();

  expect(wrapper.vm.changeScreen).toBe("Login");
});

test("El button de Ver Contactos", async () => {
  const wrapper = shallowMount(App);
  expect(wrapper.emitted().vercontactos).toBe(undefined);

  const button = wrapper.find(".firstScreen");
  button.vm.$emit("vercontactos");
  await wrapper.vm.$nextTick();

  expect(wrapper.vm.changeScreen).toBe("Listshowcontacts");
});
