<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/contact', function (Request $request) {
    $results = DB::select('select * from contact');
    return response()->json($results, 200);
});

/********Function de Añadir un persona************* */

Route::post('/contact/', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into contact (id, name, lastname, email, phone)
        values (:id, :name, :lastname, :email, :phone)
    ",
        $data
    );

    $results = DB::select('select * from contact where id = :id', [
        'id' => $data['id'],
    ]);
    return response()->json($results[0], 200);
});

/**************Function de Eliminar un persona *****************/

Route::delete('/contact/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (carNotExists($id)) {
        abort(404);
    }

    DB::delete('delete from contact where id = :id', ['id' => $id]);

    return response()->json('', 200);
});



/*****************Function de modifier un persona ******************/

Route::put('/contact/{id}', function ($id) {
    if (carNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from contact where id = :id",
        ['id' => $id]
    );

    DB::insert(
        "
        insert into contact (id, name, lastname, email, phone)
        values (:id, :name, :lastname, :email, :phone)
    ",
        $data
    );

    $results = DB::select('select * from contact where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});



// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('carNotExists')) {
    function carNotExists($id)
    {
        $results = DB::select('select * from contact where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}